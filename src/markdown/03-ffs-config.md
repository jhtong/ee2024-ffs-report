## Configuration via FFSData Struct:

An exclusive data structure `Vector_Data_t` was created to store all the readings essential for the core functionality of the system. This allowed us to keep the data more organized and paved the way for a more systematic approach towards polling the sensor readings – `update_sensorData()`. 

| Name | Data Type | Use State |
| -- | -- |
| btn_calibrate | `_Bool` | Calibration |
| zAccel | `int8_t` | Calibration & Active |
| zOff | `int32_t` | Calibration & Active |
| standbyCounter | `int32_t` | Standby |
| previousMs | `uint32_t` | Standby |
| delayMs | `uint32_t` | Standby
| light | `uint32_t` | Standby & Active |
| temperature | `float` | Standby & Active |
| isHot | `_Bool` | Standby & Active |
| isRisky | `_Bool` | Standby & Active |
| vibrationFreq | `int32_t` | Active |
| warningOn | `_Bool` | Active |
| msTicks | `uint32_t` | All States |

```c
/*
 * FFS_Config.h
 *
 *  Created on: Mar 27, 2014
 *      Author: joel
 */

#ifndef FFS_CONFIG_H_
#define FFS_CONFIG_H_

/** For temperature maximum **/
#define TEMP_MAX 38.0 //26.0

/** Offset for accelerometer */
#define GZERO_OFFSET 75.0

/** For light sensor **/
#define LIGHT_UPPER_BOUND 							800
#define LIGHT_LOWER_BOUND 							0

/** For vibration frequency bounds **/
#define FREQ_UNSAFE_LOWER 							2
#define FREQ_UNSAFE_UPPER 							6

/** For vibration calculations **/
#define 		VIBRATION_BUFFER_SIZE 				5				// size of smoothing buffer.
#define 		VIBRATION_MAX_DIFF 					5				// Max threshold dy/dx for a point

/** WARNING ----------------------------------------------------> **/
#define 		BLINK_INTERVAL 						226				// 0 - 255

/** Buzzer **/
#define 		BUZZER_FREQ							2551


#define NOTE_PIN_HIGH() GPIO_SetValue(0, 1<<26)
#define NOTE_PIN_LOW()  GPIO_ClearValue(0, 1<<26)


/** UART Stuff **/
#define 			UART_BUFF_SIZE 					256

/** Wireless protocol stuff **/
#define 			PROTOCOL_REPORT_TIME_NORMAL 	1000
#define 			PROTOCOL_REPORT_TIME_WARNING 	1000 / 8

#endif /* FFS_CONFIG_H_ */
```

