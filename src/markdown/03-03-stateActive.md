### Active

**Peripherals Used:** Acclerometer, Temperature & Light Sensor, OLED Display, LED Array, RGB LED, Buzzer, Wireless UART Interface

**Enter Handler:**

	Initialize static elements on OLED Display for Active state
    Initiate wireless UART connection

#### Safe Conditions

From an overview, the active state combines the elements of `CALIBRATION` with `STANDBY`. The temperature & light readings are actively displayed on the OLED screen, while the accelerometer readings are processed in the background. If either `isHot` or `isRisky` conditions are satisfied during this state, the program will revert back to `STANDBY`. This is probably the slowest state of the three in terms of the time taken to complete a round-robin cycle. This is because each cycle in `ACTIVE` has to read more sensors and process more data when compared to `STANDBY` or `CALIBRATION`. This means that there will be a small, albeit noticeable delay between the time when the readings become unsafe and the time at which the system actually switches states. In the context of this project, the problem doesn't have much significance. But if this was implemented as a real-time system on an aircraft, the delay could be catastrophic.

#### Warning Indicators

Our system exhibits two different means of indicating that the flutter frequency has reached an unsafe level. One of them is the UART terminal and the other involves the use of various devices on the baseboard: buzzer, LED array and RGB LED. The wireless UART communication is explained in detail in the 'Wireless Protocol Handling' section. As for the baseboard devices, two simple functions `warning_On()` & `warning_Off()` were created to control the three indicators.

**RGB LED**

The red LED was configured as a simple GPIO output pin at Port 2 with a bit value of `0x00000002`:
```c
void rgbRed_init() {
	GPIO_SetDir(2, 1 << 0, 1);
}

void rgbRed_on() {
	GPIO_SetValue(2, 1 << 0);
}

void rgbRed_off() {
	GPIO_ClearValue(2, 1 << 0);
}
```

**LED Array**

The LED array was controlled using the `pca9532` library. The mask `0xFFFF` was used to turn all the LEDs on simultaneously. `BLINK_INTERVAL` 226 roughly correlates to a frequency of 1Hz.
```c
#define  BLINK_INTERVAL	226

....

void leds_onWarning() {
	pca9532_setBlink0Period(BLINK_INTERVAL);
	pca9532_setBlink0Leds(0xFFFF);
}

void leds_offWarning() {
	pca9532_setLeds(0, 0xFFFF);
}
```

**Buzzer**
See 'PWM Simulation with External Interrupt' section

##### UML Diagram

A diagram of state Active is given below:

![sate calibrate diagram](images/pubimage_004.png)