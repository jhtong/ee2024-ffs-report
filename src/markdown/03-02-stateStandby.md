### Standby

**Peripherals Used:** Temperature & Light Sensor, OLED Display, 7-Seg Display

**Enter Handler:** 

	Reset standbyCounter to 5
    Initialize static elements on OLED Display for Standby state
    Turn off FFS warning
    Enable EINT3_IRQn (Light Sensor Interrupt)

#### Countdown

For the 5-second countdown, the `led7seg_setChar` library function was used to control the 7-segment display. Akin to the OLED refresh rate problem, it's exhaustive to set the display number repeatedly for each cycle. Furthermore, the display number doesn't change for atleast 1000ms, so most of the time precious clock cycles will be wasted writing the same value to the 7-seg. The obvious solution to this is to implement a delay function, which will wait for 1000ms before updating the 7-seg, and then continue to execute the rest of the cycle. But this is detrimental to the flow of the round-robin architecture. The delay function will hang the program for 1000ms; it's very likely that crucial data from the temperature & light sensor will be missed during this interval. So the only feasible solution is to declare a static variable `prevMs` to record the previous interval at which the 7-seg display was updated. And then we use this time reference to check if 1000ms has passed without breaking the flow of the loop:
```c
void tickHandler1() {
	....
	static uint32_t prevMs = 0;

	if ((SysTick_getMsElapsed() - prevMs) > 1000) {
    	prevMs = SysTick_getMsElapsed();
    	standby_countDown(&FFSData);
    }
    ....
}
```
The actual updating takes place in `standby_countDown` found in `functions.c`:
```c
void standby_countDown(volatile Vector_Data_t* data) {
	if (data->standbyCounter > 0) {
		data->standbyCounter--;
		led7seg_setChar((char) (((int) '0') + data->standbyCounter), FALSE);
	}
}
```
Our system doesn't initiate the handshake protocol during the `STANDBY` state as that would trigger a reinitiation everytime the state changed from `ACTIVE` to `STANDBY`.

#### Temperature and Light Sensor

The `STANDBY` mode uses a very basic implementation of the temperature sensor. The main function intializes the sensor using `temp_init()`; `update_sensorData()` then polls and stores the latest reading using `temp_read()` (in the actual implementation, the reading takes place inside a sub-fuction called `temperature_update`). Since the sensor is analog, the library function simply performs a `GPIO_ReadValue` operation at timed intervals to obtain the latest temperature.

On the contrary, the light sensor has a much more comprehensive implementation. See the 'Light Sensor Interrupt' section for more details on how the sensor was used to trigger an external interrupt. The following config function was called in `main()` to initiate the sensor:

```c
#define LIGHT_UPPER_BOUND 	800
#define LIGHT_LOWER_BOUND 	0

...

void SensorLight_config() {
	light_enable();
	light_setRange(LIGHT_RANGE_1000);

	// configure PINSEL
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = 0;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 5;
	PINSEL_ConfigPin(&PinCfg);

	// config GPIO 2.5 for temp pin
	GPIO_SetDir(2, 1 << 5, 0);

	SensorLight_setThreshold(LIGHT_LOWER_BOUND, LIGHT_UPPER_BOUND);
	SensorLight_setInterrupts();
}

void SensorLight_setThreshold(uint32_t min, uint32_t max) {
	light_setLoThreshold(min);
	light_setHiThreshold(max);
}
```
`LIGHT_RANGE_1000` is an obvious range choice since anything above 800 will be curbed by the `setThreshold` bound. The actual readings are updated and stored every 200ms:

```c
void light_update(volatile Vector_Data_t * data) {
	static uint32_t currMs = 0;
	if (currMs - SysTick_getMsElapsed() > 200) {
		FFSData.light = light_read();
		currMs = SysTick_getMsElapsed();
	}
}
```


#### Active State Transistion

If and only if `isRisky` & `isHot` are not satisfied and `standbyCounter` has reached zero, the state manager will change the state to `ACTIVE`.

```c
if (FFSData.standbyCounter == 0 && !FFSData.isRisky && !FFSData.isHot) {
    StateManager_goto(sm, 2);
    return;
}
```
The return statement indicates to the state manager that there is no need to complete the rest of the cycle since it's switching states.

##### UML Diagram

A diagram of state Standby is given below:

![sate calibrate diagram](images/pubimage_003.png)