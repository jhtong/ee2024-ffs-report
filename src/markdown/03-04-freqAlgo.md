## The FFS frequency algorithm

At the core of the FFS lies the frequency algorithm.  Our implementation of the detection system works by extracting data from the on-board accelerometer, and putting the data through an algorithm to determine the rate of vibrations per second.

### Possible algorithms for eliminating data noise

Various algorithms were considered for determining frequency. These algorithms can be categorized into those that require an IMU, versus those that requires just an accelerometer.  Our team settled for an average-sampling algortihm, due to its ease in implementation, relatively acceptable accuracy and computation speed.  The various algorithms considered are as listed below:


#### Kalman Filter

The Kalman filter appears to build an optimal model from noisy data.  It relies on recursion and the input of data, and hence has memory of previous states.  These states are then used to create a "projected model" (determining data points projected in the future). (Kleeman, 1996)

Kalman filtering is a popular method to determine the orientation of objects, using the IMU, due to its speed (linear filter), and its ability to eliminate Gaussian noise.  However, it was not chosen due to the complexity of implementing matrix operations in `C`, the perceived responsiveness of the model due to its inherent memory (frequencies might rapidly change and might not be reflected in time by the FFS system).


#### Origin / Savitzky-Golay Filtering

This filtering method involves applying polynomial regression analysis to the data, and appears to preserve peaks within the data (necessary for calculation of vibration frequency).  However, it was not implemented due to its non-linearity. (Source: http://www.originlab.com/index.aspx?go=Products/Origin/DataAnalysis/SignalProcessing/SmoothingAndFiltering&pid=104)


#### Fourier analysis and suppression using filtering

Spectral analysis of data could be performed, and frequencies stemming from noise removed using filters.  However, finding the noisy frequency may involve some machine learning (or an adaptive algorithm), and the imeplementation of a Fast Fourier Transform algorithm in C (both of which may be time- and implementation- costly).


#### Median filtering

Median filtering involves taking the middle value across $$$n$$$ sorted sampled values, at every interval.  This implementation appears to be fast and easy to implement, as it is a linear filter.  It also preserves features across data, such as peaks.  However, while this method was recommended, it was not chosen as our initial implementation appeared to be not as responsive, possibly due to memory in the system.


#### Adjacent average filtering

This method is similar to median filtering.  It involves taking the average across $$$n$$$ sampled values at every interval.  This method was chosen, in addition to some conditional implementation to further eliminate noise, as it was easy and fast to implement, responsive (little memory in the system), and did not require a sorting algorithm.


### Our choice of algorithm

After considering all possible options, we settled for the Adjacent Average Filtering (AAF) algorithm due to its ease in implementation, responsiveness, and reliability.


#### How does it work?

To understand why this algorithm works, we first have to define the meaning and detection of frequency.  Frequency entails finding the occurences of a particular event over a given period of time, taken to be 1 second.  In the FFS, features of this data involves calculating the number of minima and maxima occuring per unit time, in the plot of Displacement $$$x/mm$$$ VS Time $$$t/s$$$.  It follows that the nature of this data would emulate Simple Harmonic Motion (SHM), and would hence obey the equation:

$$
a \propto -x , \quad a = \ddot x
$$

Now, we define the reading from the accelerometer as $$$k \, \exists\,k\,\in\,\mathbb{Z} $$$.  Therefore

$$
(a \propto -x) \wedge (k \propto a) \Rightarrow k \propto -x.
$$

This means that finding the number of peaks and troughs (and hence frequency) is approximately equal to finding the number of changes of sign of $$$k$$$.  Let $$$f$$$ be the frequency of vibrations.  Then:

$$
f = \text{Number of sign changes of $k$, per second.}
$$


#### Algorithm for determining sign changes

Instead of implementing conditionals to detect for sign change, we speed up the algorithm by performing bitwise operations.  This increases the responsiveness of the system.

Let $$$a, b$$$ be binary integers, represented in two's complement and given by statement $$$C$$$:  

$$
\textbf{C}: a, b \forall (a > 0 , b < 0) \vee (a < 0, b > 0)
$$

Then it is given that:

$$
a \oplus b < 0 \quad
$$

**Proof**

Let the MSB of $$$a,b$$$ be given by $$$a_m$$$ and $$$b_m$$$ respectively.  Then let D be an ordered pair such that $$$D = (a_m, b_m)$$$.  Let E(D) be the result of the operation $$$a_m \oplus b_m$$$.

Suppose condition C holds.  Then it must follow that:

$$
D = (0,1) \wedge D = (1,0)
$$

Therefore $$$E(D) = 1 \Rightarrow a \oplus b < 1$$$.  Therefore the result holds.


Suppose condition C does not hold.  Then it must follow that:

$$
D = (1,1) \wedge D = (0,0)
$$

Therefore $$$E(D) = 0 \Rightarrow a \oplus b \geq 0$$$.  Therefore the result does not hold, as expected.

Q.E.D.


#### Implementation of algorithm in code

**Retrieving data**

This is implemented by doing an averaging of input values.  As the I2C implementation may block code execution during transmission, we request for accelerometer readings at intervals, using a timer non-blocking loop.  This is done by `readStoreAccel()` defined in `functions.c`.  The function is made as fast as possible by utilizing the customized `__INLINE` directive:

```c
__INLINE void readStoreAccel(volatile Vector_Data_t* data) {
	static uint32_t refMs = 0;
	uint32_t nowMs = SysTick_getMsElapsed();

	if(nowMs - refMs > 0) {
		int8_t xDummy = 0, yDummy = 0;
		acc_read(&xDummy, &yDummy, (int8_t *) &(data->zAccel));
		data->zAccel = data->zAccel + data->zoff;
		refMs = SysTick_getMsElapsed();
	}
}
```

**Noise filtering**

Noise filtering is done by averaging `VIBRATION_BUFFER_SIZE` samples.  This is done by the function `vibrations_getSmoothedAverage()` in `functions.c`:

```c
__INLINE int8_t vibrations_getSmoothedAverage(int8_t rawInput) {
	static int8_t buffer[VIBRATION_BUFFER_SIZE];
	static int8_t ref;

	if (ref != VIBRATION_BUFFER_SIZE) {
		buffer[ref] = rawInput;
		ref++;
		return 0;
	}

	// else return value
	ref = 0;
	uint8_t i, acc = 0;
	for (i = 0; i < VIBRATION_BUFFER_SIZE; i++)
		acc += buffer[i];
	return acc;
}
```

The buffer is made `static` within the function to preserve its state during re-entrancy (and hence promoting data encapsulation).


**Determining frequency**

Determining the frequency is handled by the function `vibrations_updateFrequency()`, with the `XOR` method imeplemented.  Note that this algorithm is very responsive as it has little memory of its previous state.  It also eliminates values that are too close to the origin, to be determined:

```c
void vibrations_updateFrequency(volatile Vector_Data_t * data) {
	static uint32_t refMs;
	uint32_t nowMs;
	static int8_t oldVal, newVal;
	static int32_t accumulator;

	if (refMs == 0)
		refMs = SysTick_getMsElapsed();

	nowMs = SysTick_getMsElapsed();

	if ((nowMs - refMs) > 1000) {
		// EXIT and reset variables if reached
		data->vibrationFreq = accumulator;
		refMs = SysTick_getMsElapsed();
		accumulator = 0;
	}

	/** do accelerometer calculations here **/

	int8_t curr = vibrations_getSmoothedAverage(data->zAccel);
	if (curr == 0)
		return; 			// exit if no data

	oldVal = newVal;
	newVal = curr;

	// we want opposing signs => a peak => freq++.
	// since number is in 2's Complement, XOR of both will set MSB to 1
	// i.e. number < 0
	if (((newVal ^ oldVal) < 0) && (abs(newVal) > VIBRATION_MAX_DIFF)) {
		accumulator++;
	}
}
```
























