## Interrupts

As far as possible, interrupts were used to retrieve data.  Two types of interrupts were deployed:

- Timer interrupts
- Sensor interrupts

While interrupts were mainly used for data retrieval purposes, they were used in our implementation of FFS, to emulate PWM on peripheral pins which were not connected via PWM.  This was achieved by bit-banging a GPIO pin.

As interrupts are potentially disruptive to the system (program sequence is interrupted and may not be as expected upon resuming the program), care was taken to ensure integrity of conditionals and data variables upon resuming the program.  Fortunately, this was not a major issue, save for the light sensor interrupt.

Our implementation of interrupts in the FFS was designed to be quick, to avoid possible disruption to the scheduling of the rest of the program.  Hence, they were as far as possible, only used to modify data variables, which were then handled upon resuming the program context, when the system was ready to handle the modified data.  Functions were also minimally used, as they tend to be comparatively longer (operations to store and retrieve the context to and from the stack have to be executed).  They were only used as a last resort to enhance code readability.


### Systick interrupt

SysTick interrupts are a method of keeping track of time in the FFS.  In our implementation, we stepped down the interrupt to trigger every 1ms.  While it is certainly possible to trigger the interrupts every 1 us, doing so would flood the FFS with unnecessary interrupts, wasting processing time and battery power.

The Systick handler is initialized and called from `FFSClock.c`, using the initializer function `init_sysTick()`.  Functions are defined in `FFSClock.h`.  While the delay timers in the header file are handy, they were used as minimally as possible as they were **blocking** calls, and would impact the responsiveness of the entire program:

**FFSClock.h**

```c
void SysTick_Handler(void);
void SysTick_delay(uint32_t delayTicks);
uint32_t SysTick_getMsElapsed(); 				// wrapper

// to call!
void init_sysTick();
```


### GPIO interrupts

GPIO interrupts are triggered using the `EINT3` interrupt handler.  They are used throughout our implementation of FFS, and involve first configuring the system utility function `NVIC_EnableIRQ()`.  Upon interrupt triggering, the `GPIOINT` register is checked for the corresponding interrupt status rising or falling flags.  These flags are then cleared, before the sensor's internal interrupt flag is cleared (by I2C means or otherwise).  Such an order is needed, to prevent deadlock from happening.

```c
void EINT3_IRQHandler() {
	if (LPC_GPIOINT->IO2IntStatF >> 5 & 1) {
		LPC_GPIOINT->IO2IntClr |= (1 << 5);
        light_clearIrqStatus();
	}

	// goto state
	FFSData.isRisky = 1;
}
```

An issue to note that is as all GPIO interrupts are triggered using the `EINT3` handler, all interrupts need to be carefully tracked and noted to prevent deadlock caused by a misplaced interrupt.


### Light Sensor Interrupt

The light sensor can be configured to trigger line interrupts.  As the interrupt lines are connected to GPIO 2.5, which supports GPIO interrupts via `EINT3`, interrupts can be configured.

Upon asserting the interrupt, the interrupt goes `PENDING` and waits until the processor is ready.  It then enters `ACTIVE` mode.  The correct pin is filtered, and if asserted, both the light sensor and NVIC interrupts are  deasserted, in that order.  The FFSData struct is then updated with the new data.

Implementation of the interrupt handler is given in the header file `SensorLight.c`:

```c
void EINT3_IRQHandler() {
	if (LPC_GPIOINT->IO2IntStatF >> 5 & 1) {
		LPC_GPIOINT->IO2IntClr |= (1 << 5);
        light_clearIrqStatus();
	}

	FFSData.isRisky = 1;
}
```

A notable problem is that when asserted, there is no interrupt to assert that the light sensor reports normal readings, if previously asserted.  Hence we implement the below.

This new data is handled when the system is ready (enters the respective state's stick function).  One problem that might result is the pre-emption of code, particularly when clearing the value of the `FFSData.isRisky` flag, resulting in deadlock.  Instead of implementing mutexes, this is overcome by checking the flag against the value of the light sensor.  The flag was cleared should both tally.

Our implementation was found to be sufficient.  The code for this checking for memory integrity is given in the function `clearLightFlag()`, defined in `states.c` and called at the end of every tick:

```c
static void clearLightFlag() {
	if (FFSData.isRisky) {
		uint32_t lightVal = FFSData.light;
		if ((LIGHT_LOWER_BOUND < lightVal) && (lightVal < LIGHT_UPPER_BOUND)) {
			FFSData.isRisky = 0;
			SysTick_delay(1);
		}
	}
}
```

### PWM emulation using External Timer interrupt

To emulate pulse-width modulation (PWM) signals, external timer inteerupts were used.  This was used to generate PWM signals for the buzzer via bit-banging.  As the buzzer was connected to GPIO 0.26 and did not have a suitable PINSEL function for PWM, this avoided the need for a blocking delay using `sysTick`, which would have slowed down the system considerably.

The External Timer interrupt was set-up using the code below, in `functions.c`:

```c
void buzzer_onWarning() {
	TIM_TIMERCFG_Type TIM_ConfigStruct;
	TIM_MATCHCFG_Type TIM_MatchConfigStruct;

	// Initialize timer 0, prescale count time of 1ms
	TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
	TIM_ConfigStruct.PrescaleValue = 1;
	TIM_MatchConfigStruct.MatchChannel = 0;
	TIM_MatchConfigStruct.IntOnMatch = TRUE;
	TIM_MatchConfigStruct.ResetOnMatch = TRUE;
	TIM_MatchConfigStruct.StopOnMatch = FALSE;
	TIM_MatchConfigStruct.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
	// Set Match value, count value is time (timer * 1000uS =timer mS )
	TIM_MatchConfigStruct.MatchValue = BUZZER_FREQ / 2;

	TIM_Init(LPC_TIM1, TIM_TIMER_MODE, &TIM_ConfigStruct);
	TIM_ConfigMatch(LPC_TIM1, &TIM_MatchConfigStruct);

	// To start timer 0
	TIM_Cmd(LPC_TIM1, ENABLE);
	NVIC_EnableIRQ(TIMER1_IRQn);

}
```

Turning on the buzzer enables the `TIMER1` interrupt.  Actual GPIO bit-baanging was done in the interrupt routine itself:

```c
void TIMER1_IRQHandler() {
	/** Buzzer stuff **/
	static _Bool toggle = 0;
	toggle ? NOTE_PIN_HIGH() : NOTE_PIN_LOW();
	toggle = !toggle;

	/** Clear interrupt **/
	TIM_ClearIntPending(LPC_TIM1, 0);

}
```

Similarily, the buzzer is switched off by disabling the timer:

```c
void buzzer_offWarning() {
	NOTE_PIN_LOW();
	TIM_Cmd(LPC_TIM1, DISABLE);
	TIM_ClearIntPending(LPC_TIM1, 0);
}
```

`TIMER1` was used as `TIMER0` appeared to be used internally by some of the peripherals.


### UART Interrupt

This is covered in the **wireless protocol** section.