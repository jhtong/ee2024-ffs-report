### Calibration

**Peripherals Used:** Acclerometer, OLED Display

**Enter Handler:** 

	Reset zOff to zero
    Initialize static elements on the OLED display for Active state
    Reset 7-Segment display to '5'
    Turn off FFS warning
    Disable EINT3_IRQn (Light Sensor Interrupt)

**Exit Handler:**

	Save zOff value
    Initialize isHot & isRisky conditions to false

##### Accelerometer Readings

A standard I2C protocol was used to read off the accelerometer values every time `update_sensorData()` was called. This was achieved by using the `acc_read` library function. The sensor was polled only during Calibration & Active state to reduce wasted clock cycles during the round-robin loop. After polling, the new value was offset using `zOff` and then stored into `zAccel`. The offset has no effect in Calibration mode as `zOff` is yet to be configured. However, the offset will be used later in Active mode to zero the z-axis reading. Before exiting the Calibration state, i.e. when `btn_calibrate` is asserted, the latest `zAccel` value will be used to calculate and store `zOff`: $$$ z\_{off} = -z\_{accel} $$$. The x & y axis readings are ignored completely during data storage and processing, but are still polled (burns clock cycles) due to `Lib_EaBaseBoard` legacy issues.

**Settings:**
	
    acc_setMode(ACC_MODE_MEASURE);
	acc_setRange(ACC_RANGE_2G);
    
Since the accelerometer readings will be used only to calculate the oscillating frequency, the actual range & accuracy of the data won't be of much significance as long as the values surpass the noise cancellation threshold (see Frequency Algorithm section). Hence `ACC_RANGE_2G` meets the requirments of the system.

##### OLED Display

During the development process we encountered a problem regarding the refresh rate of the OLED Display. Clearing and reinitiating the entire screen just to update a small portion of the display eg: the accelerometer readings, is a painstakingly slow process and causes the screen to flicker vigourously. And so, the display elements were split into two categories: static elements & active elements. For any state, the OLED Display is refreshed and loaded with static elements (eg: state name) just before entering that particular mode. And this is done only once per state. On the other hand, active elements are refreshed every round-robin cycle.

The readings were displayed on the OLED in $$$m/s^{2}$$$. The snippet below shows how the arbitary z-axis values were converted into standard units:

```c
void calibration_displayAccel(uint8_t zAcc) {
	float realAcc = (9.81 / GZERO_OFFSET) * zAcc;

	char buffer[3];
	sprintf(buffer, "%.1f", realAcc);
	oled_putString(23, 45, (uint8_t *) buffer, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
}
```

`GZERO_OFFSET` is the raw accelerometer reading (z-axis) when the sensor is placed in a stationary, upright position. In our case $$$g_{zero}\cong 64$$$. And intermediate buffer was used along with `sprintf` to convert the decimal value into a string since `oled_putString` doesn't support floating values.

##### GPIO Read Button (SW4)

A standard function called `ButtonList_attachButton` was created in `ButtonList.c` to intialize a GPIO pin as an input button:
```c
void ButtonList_attachButton(uint32_t port, uint32_t pin, void (*handler)()) {
	....
    
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = 0;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = port;
	PinCfg.Pinnum = pin;
	PINSEL_ConfigPin(&PinCfg);

	GPIO_SetDir(port, 1 << pin, 0);
    
    ....
}
```

Similarly, another function called `ButtonList_getButtonState` was setup to poll a particular GPIO pin:

```c
_Bool ButtonList_getButtonState(uint32_t port, uint32_t pin) {
	return ! ((GPIO_ReadValue(port) >> pin) & 0x01);
}
```
And so, `btn_calibrate` was determined by reading the state of GPIO P1.31 (SW4) every cycle.

##### UML Diagram

A diagram of state Calibrate is given below:

![sate calibrate diagram](images/pubimage_001.png)