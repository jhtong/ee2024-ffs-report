### Physical States

Our implementation of the FFS deploys the threee states `CALIBRATION`, `STANDBY`, and `ACTIVE` modes via the StateManager class.  These states are defined in the header file `states.c`.  Auxiliary functions supporting these states are defined in `functions.h`.  These were not defined separately, to enhance speedy code comprehension by a new user.

As the StateManager defines states as integers, the following mappings were used:

| State        | number |
|--|--|
| `CALIBRATION`       | 0 |
| `STANDBY`           | 1 |
| `ACTIVE`            | 2 |

The FFS system was designed to be a responsive embedded system (without and operating system), from the ground-up.  Hence data, feedback and control were designed utilizing the following diagram:

![diagram of sensor-uP-actuator](images/ee2024-transition-diagram.png)

This was impemented with the following design in the FFS:

![insert diagram of flow control here](images/ee2024-transition-diagram-ffs.png)

Hence, on every active state's tick, data which must be polled was performed via the function `update_sensorData()` in `states.c`.  When called, the function updates the `FFSData` struct defined in `Vector_Data.h`.  Whenever possible, interrupts were instead used to access sensor data, to ensure that data was captured and not missed.

`update_sensorData()` is intelligent to detect the current state it is in, and decides when to poll the sensors (instead of blind polling).  This is done by reading the current state the FFS system is in.

The function is given by the code below:

```c
/** Updates sensor data via polling.  This is called on every tick **/
static void update_sensorData() {

	FFSData.btn_calibrate = ButtonList_getButtonState(1, 31);

	if (sm->refNow == 0 || sm->refNow == 2) { // Read accelerometer only if in CALIBRATE OR ACTIVE
		readStoreAccel(&FFSData);
		vibrations_updateFrequency(&FFSData);
	}

	if (sm->refNow == 1 || sm->refNow == 2) { // Read temp and light only if in STANDBY OR ACTIVE
		temperature_update(&FFSData);
		light_update(&FFSData);
	}

}
```

An example of states is given by the `STANDBY` state, defined as `state 1` in the state manager:

```c
/***** State STANDBY *****/

/** Called on every tick **/
void tickHandler1() {
	update_sensorData();

	// reset back to 5 if hot
	if (FFSData.isHot)
		FFSData.standbyCounter = 5;

	if (FFSData.standbyCounter == 0 && !FFSData.isRisky && !FFSData.isHot) {
		StateManager_goto(sm, 2);
		return;
	}

	// Decrement counter for the first 5 sec:
	static uint32_t prevMs = 0;

	if ((SysTick_getMsElapsed() - prevMs) > 1000) {
		prevMs = SysTick_getMsElapsed();
		standby_countDown(&FFSData);
	}

	displayTempLux(&FFSData);
	clearFlags();
}

/** setup handler **/
void enterHandler1() {
	FFSData.standbyCounter = 5;
	standby_init();
	warning_off();
}

/** teardown handler **/
void endHandler1() {

}

/** ... **/
state_1 = State_new(tickHandler1, enterHandler1, endHandler1);
/** ... **/
```

The state diagram and relationship between the physical and wireless states is given below:

![State diagram here](images/stateDiagrams.png)