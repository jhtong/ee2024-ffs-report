## Statement of Contribution

Student 1: A0108165J
Student 2: A0105912N

**A. Joint Work in algorithm development, programming and report writing (briefly list a few specific aspects):**

Development: Active state, State manager implementation, Debugging, Algorithm tests

**B. Student 1’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):**

Development: Active state, Interrupts, Wireless UART, State manager, Algorithm design, Architecture

Report: Frequency Algorithm, Interrupts, State manager, Wireless protocol, Extensions

**C. Student 2’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):**

Calibration state, Standby state, Algorithm Experiments & Limitations, Data integrity

Report: Calibration, Standby, Active, Experiments & Limitations

We both agree that the statements above are truthful and accurate.

Signatures and Names:


---------------------------------------------

**TONG Haowen Joel (A0108165J)**

---------------------------------------------

**Mohit Shridhar (A0105912N)**

