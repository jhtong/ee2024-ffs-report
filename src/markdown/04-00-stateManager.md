## Overview

### The State Manager

We have simplified the design of FFS into states.  As these transitions are often cumbersome to represent in code, we have included a state manager design pattern to handle these transitions.

The State Manager has knowledge of all possible states.  Based on commands sent by the program, the State Manager navigates between states, ensuring that memory is properly allocated and cleared prior to the next state entry.


#### What is a state?

A `state` is similar to creating a unit test.  That is, each `state` comprises three function handlers: one function to set-up the necessayr variables prior to state entry, another function that is run during state execution on every tick, and another function that clears the state (e.g. freeing memory).  These functions are defined and passed during state creation as function pointers.

Each attached function handler has the following signature:

	void func_handler() {
		/** Do stuff here **/
		return;
	} 

The following functions are used to create a State instance.  They are defined in `State.h`:

	State_t * State_new( void (* on_tick)(), void (* on_enter)(), void (* on_exit)()  );
	void State_delete(State_t * state);


#### Usage of the State Manager

The State Manager instance is first created.  Then, states are added to it.  The State Manager is then "kicked" to initialize the first state, and run.  The following flowchart shows usage of the State Manager:

![insert flowchart here showing usage](images/ee2024-state-mgr-class.png)

For the state manager to run on each tick, an "onTick" function is exposed in the StateManager API.  This is enclosed by an indefinite loop.

The following code snippet illustrates deployment of the state manager:


	/** Example of running State Manager **/

	StateManager_t * sm = StateManager_new();

	State_t * state0 = State_new(setup0, tick0, teardown0);
	State_t * state1 = State_new(setup1, tick1, teardown1);

	StateManager_addState(sm, state0);
	StateManager_addState(sm, state1);

	StateManager_kick(sm);

	while(1) {
		StateManager_tick(sm);
	}

	/** Never reaches here **/
	StateManager_delete(sm);
	State_delete(state0);
	State_delete(state1);


#### Navigating between states

To transit to a new state, use `StateManager_goto()`.  The function has the following signature:

	void StateManager_goto(StateManager_t * sm, uint32_t ref);

A sequence diagram illustrates one such transition, in full detail:

![insert transition here](images/ee2024-state-transitions.png)


#### State Manager API

The following publicly-accessible functions are included:

	/** Public-accessible functions in StateManager.h **/
	StateManager_t * StateManager_new();
	void StateManager_delete(StateManager_t * sm);

	void StateManager_addState(StateManager_t * sm, State_t * state);
	void StateManager_kick(StateManager_t * sm);
	void StateManager_tick(StateManager_t * sm);
	void StateManager_goto(StateManager_t * sm, uint32_t ref);
















