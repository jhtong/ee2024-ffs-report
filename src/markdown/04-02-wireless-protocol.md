## Wireless protocol handling

In line with good design practice, wireless protocol handling is abstracted as a series of states at the top level.  For the FFS, we have designed these two states as: (1) The Authentication state, (2) The Active State.  Upon completing the Authentication State, the manager transits to the Active State, and remains there.

The implementation of low-level communication is separated from the high-level implementation of the protocol.  In the FFS, low-level communication via the XBee UART is handled by a `Terminal` class singleton, while the high-level protocol is handled by a `StateManager` instance.  These two objects are loosely coupled to each other, in line with good design. 


### Terminal singleton

The Terminal singleton can be thought of as a manager that handles the transmitting, receiving and parsing of messages via the UART.  In the design of the FFS, the Terminal has memory of the last received message (if not retrieved) and the last transmitted message.

The following functions are exposed for controlling the Terminal singleton:

```c
/** Creation functions **/
Terminal_t * Terminal_new();
void Terminal_delete(Terminal_t * self);

/** Must be attached to interrupt handler **/
void Terminal_harvest(Terminal_t * self, uint8_t * buffer, uint32_t length);

/** Other functions to play around with **/
_Bool Terminal_hasMessage(Terminal_t * self);
void Terminal_send(Terminal_t * self, const char * payload);
uint32_t Terminal_getTimeElapsed(Terminal_t * self);
Message_t * Terminal_retrieve(Terminal_t * self);
void Terminal_resetTime(Terminal_t * self);
```

The class diagram is given by the below:

![Wireless Class diagram here](images/ee2024-wirelessClassDiagrams.png)

#### Messages

Messages are the foundation of all UART transmissions.  A message is defined in the struct below:


```c
typedef enum {
    RECIPIENT_FFS,
    RECIPIENT_BASE
} Author_t;

typedef struct {
    Author_t to;
    Author_t from;
    uint8_t length;
    char ** tokens;
} Message_t;
```

The following functions enable the creation and deletion of messages.  As messages are made up of string arrays (each being a byte), strings are memory-heavy and must be properly handled to eliminate major memory leaks:

```c
/** Public functions for Message class **/
Message_t * Message_new(Author_t to, Author_t from, const char * payload);
void Message_delete(Message_t * self);
char * Message_get(Message_t * self, _Bool addNewline);
```


Typically, these do not need to be called manually as they are hidden by the Terminal class.


#### Receiving messages via the Terminal singleton

Receiving messages via the Terminal class are designed in the FFS to be implementation-agnostic.  That is, messages can be either done via polling or interrupts.  In our implementation however, interrupts with data read in chunks are used, to prevent loss of information.  This is achieved by the function `UART_receive()` defined in `lpc_17xx_uart.h`, and the interrupt handler `UART3_IRQHandler()`.


##### Receiving UART data via interrupt triggering

UART interrupts are first enabled through a routine `PINSEL` configuration and a UART configuration vector.  `UART3` is used:

```c
static void pinsel_uart3(void) {
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = 2;
	PinCfg.Pinnum = 0;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 1;
	PINSEL_ConfigPin(&PinCfg);
}

void init_uart(void) {
	UART_CFG_Type uartCfg;
	uartCfg.Baud_rate = 115200;
	uartCfg.Databits = UART_DATABIT_8;
	uartCfg.Parity = UART_PARITY_NONE;
	uartCfg.Stopbits = UART_STOPBIT_1;
	pinsel_uart3();
	/**..............**/
	/**..............**/
}
```

To ensure that messages are not jumbled up, a first-in-first-out (FIFO) queue is used.  This is enabled by the following lines:

```c
// use fifo structure
UART_FIFO_CFG_Type fifoCfg;
UART_FIFOConfigStructInit(&fifoCfg);
UART_FIFOConfig(LPC_UART3, &fifoCfg);
```

Unlike most peripherals, UART is not handled in the standard NVIC interrupt handler.  Instead, a UART interrupt handler is exposed by the API.  This handler has to be called in the function `UART3_IRQHandler()`.

For the UART callback function to utilize a custom callback function defined by the user, the function handler has to be attached to the UART instance by:

	UART_SetupCbs(LPC_UART3, 0, uart_rcv_callback);

The IRQ is then enabled:

```c
NVIC_EnableIRQ(UART3_IRQn);

The code is hence:

void UART3_IRQHandler() {
	UART3_StdIntHandler();
}

// workaround to create a passable instance to interrupt callback
static Terminal_t * termSingleton;
static char buffer[UART_BUFF_SIZE];

static void uart_rcv_callback() {
	memset(buffer, 0, UART_BUFF_SIZE); 				// clear buffer first
    uint32_t len = UART_Receive(LPC_UART3, (uint8_t *) buffer, (uint32_t) UART_BUFF_SIZE, NONE_BLOCKING);
    Terminal_harvest(termSingleton, (uint8_t *) buffer, len);
}

static void pinsel_uart3(void) {
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = 2;
	PinCfg.Pinnum = 0;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 1;
	PINSEL_ConfigPin(&PinCfg);
}

void init_uart(void) {
	UART_CFG_Type uartCfg;
	uartCfg.Baud_rate = 115200;
	uartCfg.Databits = UART_DATABIT_8;
	uartCfg.Parity = UART_PARITY_NONE;
	uartCfg.Stopbits = UART_STOPBIT_1;
	//pin select for uart3;
	pinsel_uart3();
	UART_Init(LPC_UART3, &uartCfg);
	UART_SetupCbs(LPC_UART3, 0, uart_rcv_callback);
	UART_TxCmd(LPC_UART3, ENABLE);
	UART_IntConfig(LPC_UART3, UART_INTCFG_RBR, ENABLE);

	// use fifo structure
	UART_FIFO_CFG_Type fifoCfg;
	UART_FIFOConfigStructInit(&fifoCfg);
	UART_FIFOConfig(LPC_UART3, &fifoCfg);

	NVIC_EnableIRQ(UART3_IRQn);
}
```


#### Harvesting data

To collect data on each interrupt, we have defined a function handler `Terminal_harvest()`.  This function is embedded and called within the function handler `uart_rcv_callback()` (The function handler which is attached to the UART).


#### Sequence diagram

The following sequence diagram illustrates the calling of these functions, on an interrupt.

![Insert SD here](images/ee2024-uart-interrupt-sd.png)


#### Retrieving messages

To retrieve messages, the data handler `Terminal_harvest()` must first be attached to the interrupt handler. 

![insert mailbox picture here](http://www.tpedesign.net/wp-content/uploads/2012/09/mailbox.jpg)

Source: http://www.tpedesign.net/wp-content/uploads/2012/09/mailbox.jpg

The latest message instance is stored in the Terminal and accessible via `Terminal_retrieve()`.  It operates similar to a mailbox; Once read, the message pointer is freed.  Hence it is important to store the pointer returned by `Terminal_retrieve()`.  Both the functions `Terminal_hasMessage()` and `Terminal_retrieve()` are non-blocking.

An example usage for message retrieval is given by the code below:

```c
extern Terminal_t * term; 	// pointer to Terminal instance

if (Terminal_hasMessage(term)) {
	Message_t * msg = Terminal_retrieve(term);
	/** Do something with message here **/
	/** .............................. **/
}
/** Non-blocking                     **/
/** Continues if no message is found **/
```

#### Sending messages

Sending messages are exposed by the function `Terminal_send()`.  It is called as follow:

```c
extern Terminal_t * term; // pointer to Terminal instance
Terminal_send(term, "some message string") // auto-appends \r\n or \n
```

#### Checking for time of last message

Timeouts and time intervals are important for messages.  Hence we expose the methods `Terminal_resetTime()` and `Terminal_getTimeElapsed()`, which resets the time of the last message and returns the time elapsed since the tranmission or sending of the last message, respectively.  `Terminal_resetTime()` is automatically called when either `Terminal_send()` or a new message (triggered by interrupt) is received.


### The Wireless State Manager: Implementing the wireless protocol

In line with good protocol design, the protocol is modeled after a series of two states: `AUTH` and `ACTIVE`.  In the `AUTH` state, handshaking is attemped and performed.  The state manager than proceeds to the `ACTIVE` state, which implements the rest of the protocol.

The following state diagram illustrates these transitions:

![](images/stateDiagrams.png)

The states are deployed with our earlier implementation of the `StateManager` class.  As the function `StateManager_tick()` exposes a micro-timeslice of code execution, seemingly parallal task execution is achieved with the indefinite loop handler in `main.c`:

```c
/** Run the managers in indefinite loop **/

/** wm - wireless manager **/
/** sm - state manager    **/
extern StateManager_t * wm;
extern StateManager_t * sm;

while (1) {
	StateManager_tick(wm);
	StateManager_tick(sm);
}
```

The wireless states are hence "independent" and decoupled from the physical states.


Wireless states are defined in the header file `statesWireless.h`.  These files utilize the physical abstraction `Terminal` class, and in doing so, keep low-level implementation away from the protocol layer.

The simplicity of this design is beautifully illustrated by code in the `AUTH` tick handler:

```c
extern StateManager_t * wm;

void wState_authenticateTick() {
    //timeout 
    if(Terminal_getTimeElapsed(tm) > 5000 && !Terminal_hasMessage(tm)) {
        Terminal_resetTime(tm);
        Terminal_send(tm, "RDY 013");
    }

    // return if no message
    if (!Terminal_hasMessage(tm)) return;

    // else continue
    lastMsg = Terminal_retrieve(tm);
    char ** tokens = lastMsg->tokens;
    
    if (strcmp(tokens[0], "RNACK") == 0) {
    	puts("not acknowledged.");
        Terminal_send(tm, "RDY 013");
    }

    else if (strcmp(tokens[0], "RACK") == 0) {
    	puts("acknowledged!!!");
        Terminal_send(tm, "HSHK 013");
        StateManager_goto(wm, 1);		// goto ACTIVE state
    }

    Message_delete(lastMsg);
}
```

#### Sending ping data

Data is sent at ping intervals, when the sufficient timeout $$$t = \frac {1000} 8$$$ or $$$t = 1000$$$ ms happens.

Frequency data is accessed from the `FFSData` struct, defined in the file `Vector_Data.h`.

Ping data is sent by the utility function `pingIfNeeded()`:

```c
static void pingIfNeeded() {
	uint32_t interval;
	interval = FFSData.warningOn ? PROTOCOL_REPORT_TIME_WARNING : PROTOCOL_REPORT_TIME_NORMAL;

	static uint32_t lastPingTime = 0;
	if ((SysTick_getMsElapsed() - lastPingTime) < interval)
		return;
	lastPingTime = SysTick_getMsElapsed();

	char * reportStr = makeReportString();
	Terminal_send(tm, reportStr);
	free(reportStr);
}
```

#### Navigating between `CALIBRATE` and `STANDBY` physical states

In line with structured OOP design, This is made possible by calling the function `StateManager_goto()` on the physical state manager, which transits states on the next tick (proper deallocation of memory).  An example of this is given by the `ACTIVE` state tick handler:

```c
void wState_activeTick() {
	pingIfNeeded(); 				// send data over

	if (!Terminal_hasMessage(tm)) return;

    // else continue
    lastMsg = Terminal_retrieve(tm);
    char ** tokens = lastMsg->tokens;

    if (strcmp(tokens[0], "RSTC") == 0) {
        Terminal_send(tm, "CACK");
    	// Goto calibrate mode
        StateManager_goto(sm, 1);

    }

    if (strcmp(tokens[0], "RSTS") == 0) {
        Terminal_send(tm, "SACK");
    	// Goto reset mode
        StateManager_goto(sm, 0);
    }

    Message_delete(lastMsg);
}
```

### Integrated Testing of the Wireless protocol

Automated integrated testing of the wireless protocol was implemented through a NodeJS Javascript program, written in Coffeescript shorthand.  This script enabled us to model a test "base-station", and saved us development time.

To test for memory leaks in the system, particularly with the Terminal class as it handles string pointers in memory, this script was run concurrently with the system over a long period of time, with repeated transmissions.  All memory leaks were patched.  As of the stable release, the wireless protocol manager did not appear to have any memory leaks with the UART manager.

The script is located in a separate repository, labelled `ee2024-ffs-uart-base`.  To execute the script, first install `NodeJS` and `coffee`.  Then install dpeendencies and run it via:

```bash
$ cd ee2024-ffs-uart-base
$ npm install
$ cd src && coffee ./main.coffee
```

The Coffeescript is given below:

**main.coffee**

```coffeescript
serialport = require('serialport')
SerialPort = serialport.SerialPort 

sp = new SerialPort '/dev/ttyUSB0', {
    baudrate : 115200,
    parser: serialport.parsers.readline("\n")
}

ackCtr = 0
auth = false;

procAuth = (tokens) ->

    return if not tokens

    if tokens[0] is 'RDY' and ackCtr isnt 3
        sp.write 'RNACK\r'
        ackCtr++

    else if tokens[0] is 'RDY' and ackCtr is 3
        sp.write 'RACK\r'

    else if tokens[0] is 'HSHK' 
        console.log 'reached hshk'
        auth = true

## main body

sp.open ->
    console.log 'open'
    sp.write 'test string\r\n', (err, results) ->
        console.log 'done'

    sp.on 'data', (data) ->
        console.log "Data: #{data}"
        tokens = data.split(' ').slice(0, -1)
        console.log tokens                      # dump

        return procAuth(tokens) if not auth

counter2 = false

doActiveStuff = ->
    console.log 'ping!'
    counter2 = !counter2
    
    if counter2 then sp.write 'RSTS\r'
    else sp.write 'RSTC\r'

setInterval ->
    doActiveStuff() if auth
    console.log 'ping'
, 3000

console.log 'done'
```

Further development on the system should run this tests to check for correct functioning of the system.
