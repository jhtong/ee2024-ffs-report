### Experiments & Limitations

Various experiments were carried out to test the perfomance of the frequency analysis algorithm and the reliability of the system itself. However, it's important to note that the following observations are purely qualitative; we lack the proper equipment to simulate known frequencies with reasonable precision.

####Warning Response Delay

While using the system in `ACTIVE` mode, there is a noticeable delay between the time when the user starts shaking the baseboard and the time at which the unsafe warnings go off. Most often, there will be a delay of atleast 1000ms before the system triggers a response. This is because the accumulated oscillation counts are stored in `vibrationFreq` once every `FREQ_UPDATE_PERIOD`, i.e. 1000ms.

```c
void vibrations_updateFrequency(volatile Vector_Data_t * data) {
	....
    if ((nowMs - refMs) > FREQ_UPDATE_PERIOD) {
        // EXIT and reset variables if reached
		data->vibrationFreq = round(accumulator * (1000.0 / FREQ_UPDATE_PERIOD));
		refMs = SysTick_getMsElapsed();
		accumulator = 0;
    }
    ....
}
```
This period can be obviously reduced to improve the response time, but it has been observed that the system becomes more suseptible to noise by doing so. The accelerometer is not an analog sensor, so the readings change at fixed intervals. And so, reducing `FREQ_UPDATE_PERIOD` results in a less reliable frequency count since fewer data points can be used to verify the apparent state. Increasing `FREQ_UPDATE_PERIOD` would make the frequency estimation more accurate, however 1000ms seems to be the optimum period to strike a balance between accuracy and speed.

####Smoothing Buffer Size

Similar to the previous section, changing the size of the smoothing buffer also affects the reponse time and the reliability of the warnings. One distinct observation that can be made by increasing the buffer size is its obvious effect on delaying the warning response time. So at considerably large buffer sizes, the baseboard had to be shaken for a longer period of time to trigger the alert state. This is an expected phenomenon, since the buffer size directly affects the accumulation count. For example, if the size is set to 5 and all the readings inside the buffer are zero except for the fifth one, then the calculated average is likely to be below the noise cancelling threshold. And thus, a valid direction change will be ignored by the algorithm, resulting in a slower accumulation count. Hence leading to a slower response time.

####Safety Bounds

The 'unsafe' bounds specified in the project manual seem to be quite effective in demonstrating the flutter frequency warning system.

```c
#define FREQ_UNSAFE_LOWER	2
#define FREQ_UNSAFE_UPPER    10
```
From first hand experience, it's very easy to breach `FREQ_UNSAFE_LOWER`. Shaking the baseboard at a reasonable pace will set off the warning indicators. However, reaching `FREQ_UNSAFE_UPPER` seems to be a considerable physical challenge. After constantly manipulating `FREQ_UNSAFE_LOWER`, the highest physical frequency we could attain was between 3-5Hz, even with a small smoothing buffer size of 2. Besides the obvious physical limitation of our feeble arms, there is also a fundamental constraint imposed by the sensor itself. The update interval between each accelerometer reading depends on the speed of the round-robin loop (which is affected by other sensors and data processing) and also on the sensor itself. So it's very likely that the system will constantly miss crucial direction changes at high frequecies, as it won't be ready to handle the new data. So it would be reasonable to conclude that this system is more reliable for judging if the flutter frequency is above the lower bound rather than checking if it's above the upper bound.


####Noise Cancelling Threshold

The size of `VIBRATION_MAX_DIFF` directly correlates to the amount of perceived noise, which is ignored by the algorithm. If set as small values like 1 or 2, the sensentivity of the system becomes so substantial that gentle prodding (at a considerable frequency) sets off the warning. However, occasionally the device does produce some 'phantom warnings' even if the board is placed in a stationary, upright position. So the threshold must be carefully adjusted to meet the specific requirements of the application. Setting it to a high value won't be a safe option either. Because we have to consider the fact that the z-axis offset is a numerical manipulation; not a hardware adjustment. So half of the $$$2g$$$ range has already been used. Setting the threshold beyond $$$1g$$$ will result in an unresponsive system. Based on trial & error, the optimum value for `VIBRATION_MAX_DIFF` (for the conditions specified in the manual) was found to be 5.


