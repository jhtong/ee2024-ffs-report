## Extensions

### State Manager

Effective, encapsulated design patterns, good programming practice, and real-time embedded operating systems principles, were used to design the system from the ground-up.  We used a state manager to ensure that memory and transitions between states were effectively handled and deployed.

### XBee Wireless

Design patterns were used to encapsulate the Xbee wireless module unit, instead of raw, exposed strings that may be prone to memory-leaks.

### Automated integration testing with external scripts

Automated integration testing for the Xbee (emulating a base station) was developed using NodeJS in Coffeescript, and executed.  This enables systems to be tested faster and more effectively.

### Usage of interrupts

External timer interrupts on `TIMER1` were used to generate a PWM signal for the buzzer (bit-banging) as lines were not available, and to prevent hanging of the overall system.

The `EINT0` external interrupt was also used to handle the RESET capability of the system via a pushbutton interrupt.

In addition to interrupt handling, care was also taken to preserve data integrity across pre-emptions.  Checks (similar to a real-time operating system) were performed before entering the critical section.

