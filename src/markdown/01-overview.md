## Overview

The FFS is a frequency-checking onboard system for determining the external temperature, radiation and wing flutter frequency of an aircraft.  This system is necessary to prevent aircraft disintegration from wing resonance, otherwise known as wing flutter.

We have designed our own implementation of the FFS, that is responsive and designed with Object-Orientated Programming (OOP) principles in mind.  This is a solid solution to the problem at hand, as the code can be incrementally developed and recycled in future.  Besides environmental sensing, our implementation comes with an integrated task manager that emulates the simultaneous running of concurrent tasks at once.  This is made possible and encapsulated with a state manager, that has been generated and tested on both GCC-based and ARM-based toolchains. 

Wireless protocol integration testing is automated and performed with `NodeJS` Coffeescript.

A generic state diagram is shown below:

![](images/stateDiagrams.png)